import numpy as np

from raw import *

import time
import sys

def getFM(open1):
    fm = np.zeros(shape=(1,8))
    m1 = np.zeros(shape=(1,100))
    m2 = np.zeros(shape=(1,100))
    m3 = np.zeros(shape=(1,100))
    m4 = np.zeros(shape=(1,100))
    m5 = np.zeros(shape=(1,100))
    m6 = np.zeros(shape=(1,100))
    m7 = np.zeros(shape=(1,100))
    m8 = np.zeros(shape=(1,100))

    for x in range(0,100):
	    m1[0,x]=open1[x,0]
	    m2[0,x]=open1[x,1]
	    m3[0,x]=open1[x,2]
	    m4[0,x]=open1[x,3]
	    m5[0,x]=open1[x,4]
	    m6[0,x]=open1[x,5]
	    m7[0,x]=open1[x,6]
	    m8[0,x]=open1[x,7]
    
    #for testing purpose
    #print(m1)
    #print(m2) 
    #print(m7)
    #print(m8) 
	
    fm=np.array([[np.mean(m1),np.mean(m2), np.mean(m3),np.mean(m4), np.mean(m5),np.mean(m6),np.mean(m7),np.mean(m8)]])
    return fm
	
